﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VideoDirectory.Models;
using VideoDirectory.ViewModel;
using System.Data.Entity;



namespace VideoDirectory.Controllers
{
    public class MoviesController : Controller
    {
        // GET: Movies/Random
        //public ViewResult Random()
        //{
        //    var movie = new Movie() { Name = "GooDNewS!!" };
        //    var Customer = new List<Customer>
        //    {
        //        new Customer{ Name ="Ankit"},
        //        new Customer{ Name ="Swati" }
        //    };
        //    var viewModel = new RandomMovieViewModel
        //    {
        //        Movie = movie,
        //        Customer=Customer
        //    };
        //    return View(viewModel);
        //}

        //public ActionResult Edit(int id)
        //{
        //    return Content("id = " + id);
        //}
        private ApplicationDbContext _context;
        public MoviesController()
        {
            _context = new ApplicationDbContext();
        }

        public ActionResult Index()
        {
            var movie = _context.Movies.Include(c=>c.Genre);
          
            return View(movie);
        }

        public ActionResult Details(int Id) {
            var movie = _context.Movies.Include(c => c.Genre).SingleOrDefault(c=>c.Id==Id);
            return View(movie);
        }
        //private IEnumerable<Movie> GetMovies()
        //{
        //    return new List<Movie>
        //    {
        //        new Movie{Id=1,Name="Kuch Kuch Hota Hai"},
        //        new Movie{ Id=2,Name="Dilwale"}
        //    };

        //}

        //[Route("movies/released/{year}/{month}")]
        //public ActionResult ByReleaseDate(int year, int month)
        //{
        //    return Content(year+ "/" + month);

        //}

            }
}