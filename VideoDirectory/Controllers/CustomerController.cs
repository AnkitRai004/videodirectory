﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using VideoDirectory.Models;

namespace VideoDirectory.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        private ApplicationDbContext _context;
        public CustomerController()
        {
            _context = new ApplicationDbContext();
        }
        public ActionResult Index()
        {
            var Customers = _context.Customers.Include(c => c.MembershipType);
            return View(Customers);
        }

        //private IEnumerable<Customer> GetCustomers()
        //{
        //    return new List<Customer>
        //    {
        //        new Customer{Id=1,Name="Ankit"},
        //        new Customer{Id=2,Name="Swati"}
        //    };
        //}
        //GET: Customer/Details
        public ActionResult Details(int Id)
        {
            var customer = _context.Customers.Include(c=>c.MembershipType).SingleOrDefault(c => c.Id==Id);

            if (customer == null)
                return HttpNotFound();
            
            return View(customer);
        }

       
    }
}