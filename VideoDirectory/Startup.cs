﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VideoDirectory.Startup))]
namespace VideoDirectory
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
