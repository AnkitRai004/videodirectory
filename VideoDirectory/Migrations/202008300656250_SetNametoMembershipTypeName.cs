namespace VideoDirectory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetNametoMembershipTypeName : DbMigration
    {
        public override void Up()
        {
            Sql("Update MembershipTypes set Name='Pay as you Go' where id = 1");
            Sql("Update MembershipTypes set Name='Monthly Subscription' where id = 2");
            Sql("Update MembershipTypes set Name='Quaterly Subscription' where id = 3");
            Sql("Update MembershipTypes set Name='Yearly Subscription' where id = 4");
        }
        
        public override void Down()
        {
        }
    }
}
